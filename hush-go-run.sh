#!/usr/bin/env bash
# This script is to run the lightwalletd go frontend

# Set your Hush-lite variables before you run it :)
YOURWEBSITE="lite.YOUR-WEBSITE"
USER_HOME="/home/YOUR-USERNAME"
PORT="9067"

cd $USER_HOME/lightwalletd/cmd/server   # change to where it is

go run main.go -bind-addr 127.0.0.1:$PORT -conf-file $USER_HOME/.komodo/HUSH3/HUSH3.conf -tls-cert /etc/letsencrypt/live/$YOURWEBSITE/fullchain.pem -tls-key /etc/letsencrypt/live/$YOURWEBSITE/privkey.pem
